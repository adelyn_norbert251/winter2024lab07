import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.numberOfCards = 52;
		this.cards = new Card[numberOfCards]; //you created card values in the deck
		String[] suits = {"Hearts","Diamonds","Spades","Clubs"};
		String[] values = {"1", "2","3","4","5","6","7","8","9","10","11","12","13"};
		this.rng = new Random();
		
		int i = 0;
		for(int v = 0; v < values.length; v++){ //for every card in the deck you give it a value and a suit
			for(int s = 0; s < suits.length; s++){
				this.cards[i] = new Card (values[v], suits[s]);
				i++;
			}
		}
	}
	public int lengthOfDeck(){ //this method simply wants to know how many cards you got PERIOD
		return this.numberOfCards;
	}
	public Card drawTopCard(){
		this.numberOfCards = this.numberOfCards -1;
		Card topCard = this.cards[this.numberOfCards];
		return topCard;
	}
	public String toString(){
		String allCards="";
		for(int i = 0; i < this.numberOfCards; i++){
			allCards += this.cards[i] + "\n";
		}
		return allCards;
	}
	public void shuffle(){
		for(int i = 0; i < this.cards.length;i++){
			int randoPosition = rng.nextInt(numberOfCards);
			Card copyPosition = cards[i];
			cards[i] = cards[randoPosition];
			cards[randoPosition] = copyPosition;
		}
	}
}